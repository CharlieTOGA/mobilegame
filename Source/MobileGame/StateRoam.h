// Fill out your copyright notice in the Description page of Project Settings.

#pragma once

#include "CoreMinimal.h"
#include "AbstractState.h"

#include "StateRoam.generated.h"

/**
 * 
 */
UCLASS(meta = (BlueprintSpawnableComponent), Blueprintable)
class MOBILEGAME_API UStateRoam : public UAbstractState
{
	GENERATED_BODY()

public:
	UStateRoam(const FObjectInitializer& ObjectInitializer);

	void Execute(AEnemyAI* Enemy);	//Roam Execute behaviour
	class UStateChase* stateChase;												//figure out the issue with state chase and roam including each other
};