// Fill out your copyright notice in the Description page of Project Settings.

#pragma once

#include "Math/Vector.h"
#include "CoreMinimal.h"
#include "StateRoam.h"
#include "StateChase.h"
#include "GameFramework/Character.h"
#include "EnemyAI.generated.h"

UCLASS()
class MOBILEGAME_API AEnemyAI : public ACharacter
{
	GENERATED_BODY()

public:
	// Sets default values for this character's properties
	AEnemyAI(const FObjectInitializer& ObjectInitializer);

	UPROPERTY(BlueprintReadWrite)
		bool bIsPlayerDetected;
	UPROPERTY(BlueprintReadWrite)
		float distanceToPlayer;
	UPROPERTY(BlueprintReadWrite)
		float damageDealt;
	UPROPERTY(BlueprintReadWrite)
		TArray<AActor*> waypoints;

	UAbstractState* cState;
	UStateRoam* rState;

	int currentWaypoint = 0;
	int waypointArrSize = 0;

	void StateChange(class UAbstractState* newState);

protected:
	// Called when the game starts or when spawned
	virtual void BeginPlay() override;

public:	
	// Called every frame
	virtual void Tick(float DeltaTime) override;

	// Called to bind functionality to input
	virtual void SetupPlayerInputComponent(class UInputComponent* PlayerInputComponent) override;
};
