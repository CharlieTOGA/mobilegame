// Fill out your copyright notice in the Description page of Project Settings.


#include "StateChase.h"
#include "EnemyAI.h"
#include "MobileGameCharacter.h"
#include "Kismet/GameplayStatics.h"
#include "GameFramework/Actor.h"
#include "EngineUtils.h"
#include "EnemyAIController.h"
#include "AIController.h"

void UStateChase::Execute(AEnemyAI* Enemy)
{
	AMobileGameCharacter* Player = Cast< AMobileGameCharacter>(UGameplayStatics::GetPlayerCharacter(GetWorld(), 0));
	while (Enemy->bIsPlayerDetected == true) 
	{
		Cast<AEnemyAIController>(Enemy->GetController())->MoveToActor(Player, 0);

		if (Enemy->bIsPlayerDetected == false)
		{
			//Enemy->StateChange(stateRoam);
		}
	}

	//for (TActorIterator<class AEnemyAI> ActorItr(GetWorld()); ActorItr; ++ActorItr)
	//{
	//	ActorItr->GetController()
	//}


	/*if (Enemy->bIsPlayerDetected == true)
	{
		Enemy->StateChange(new StateChase());
	}*/



}