// Fill out your copyright notice in the Description page of Project Settings.


#include "StateRoam.h"
#include "StateChase.h"
#include "EnemyAI.h"
#include "EnemyAIController.h"
#include "GameFramework/Actor.h"
#include "GameFramework/Pawn.h"
#include "EngineUtils.h"
#include "Engine.h"					
#include "UObject/UObjectGlobals.h"
#include "UObject/Object.h"
#include "Kismet/GameplayStatics.h"	


UStateRoam::UStateRoam(const FObjectInitializer& ObjectInitializer)
	: Super(ObjectInitializer)
{
	stateChase = CreateDefaultSubobject<UStateChase>(TEXT("chase state"));
}

void UStateRoam::Execute(AEnemyAI* Enemy)
{
	if (Enemy->bIsPlayerDetected == false)
	{
		UE_LOG(LogTemp, Warning, TEXT("where are you?"));
		UGameplayStatics::GetAllActorsOfClass(GetWorld(), ATargetPoint::StaticClass(), Enemy->waypoints);				//UGamplayStatics function to get all of the target points in the level
		Enemy->waypointArrSize = Enemy->waypoints.Num();																		//initialising waypoint array to number of waypoints

		if (Enemy->currentWaypoint <= Enemy->waypointArrSize)
		{
			Cast<AEnemyAIController>(Enemy->GetController())->GoToNextWaypoint();
		}
	}
	else
	{
	UE_LOG(LogTemp, Warning, TEXT("I see you"));
	Enemy->StateChange(stateChase);
	}


	/*if (Enemy->bIsPlayerDetected == true)
	{
		Enemy->StateChange(new StateChase());
	}*/



}