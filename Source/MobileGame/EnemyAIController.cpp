// Fill out your copyright notice in the Description page of Project Settings.


#include "EnemyAIController.h"
#include "EnemyAI.h"

#include "Engine.h"						
#include "Engine/World.h"
#include "Kismet/GameplayStatics.h"			

#include "Perception/AIPerceptionComponent.h"
//#include "Perception/AISenseConfig_Hearing.h"
#include "Perception/AISenseConfig_Sight.h"
//#include "Perception/AISense_Hearing.h"
#include "Perception/AISense_Damage.h"
#include "Perception/AISenseConfig_Touch.h"



AEnemyAIController::AEnemyAIController()
{
	PrimaryActorTick.bCanEverTick = true;

	AiSightRadius = 800.f;
	AiFieldOfView = 105.f;
	AiSightAge = 5.0f;
	AiOutOfSightRadius = 50.0f;
	AiConeAngle = 105.f;

	//Touch = CreateDefaultSubobject<UAISenseConfig_Touch>(TEXT("Touch"));
	//Hearing = CreateDefaultSubobject<UAISenseConfig_Hearing>(TEXT("Hearing"));
	Sight = CreateDefaultSubobject<UAISenseConfig_Sight>(TEXT("Sight"));						//creating the sightconfiguration
	SetPerceptionComponent(*CreateDefaultSubobject<UAIPerceptionComponent>(TEXT("Perception Component")));	//configuring the perception component

	//Touch
	//Hearing->HearingRange = AiHearingRadius;																		//setting AI hearing radius
	//Hearing->SetMaxAge(AiHearingAge);
	Sight->DetectionByAffiliation.bDetectEnemies = true;		//*		
	Sight->DetectionByAffiliation.bDetectFriendlies = true;	//initialising all detections 
	Sight->DetectionByAffiliation.bDetectNeutrals = true;		//*

	//Hearing->DetectionByAffiliation.bDetectEnemies = true;														//setting AI to detect all characters
	//Hearing->DetectionByAffiliation.bDetectFriendlies = true;														//
	//Hearing->DetectionByAffiliation.bDetectNeutrals = true;
	Sight->SightRadius = AiSightRadius;						//initialising the sight config radius to defined radius
	Sight->LoseSightRadius = AiOutOfSightRadius;				//initialising the sight config lose sight radius to defined lose sight radius
	Sight->PeripheralVisionAngleDegrees = AiFieldOfView;		//initialising the sight config peripheral vision to the defined view angle of the AI
	Sight->SetMaxAge(AiSightAge);								//initialising the sight config max age to the defined sight age value

	GetPerceptionComponent()->SetDominantSense(*Sight->GetSenseImplementation());						//initialising dominant sense to sight
	GetPerceptionComponent()->OnPerceptionUpdated.AddDynamic(this, &AEnemyAIController::PawnDetected);	//change from tutorial, had to give it the address of the pointer
	GetPerceptionComponent()->ConfigureSense(*Sight);
	//GetPerceptionComponent()->ConfigureSense(*Hearing);
}

void AEnemyAIController::OnMoveCompleted(FAIRequestID RequestID, const FPathFollowingResult& Result)
{
	Super::OnMoveCompleted(RequestID, Result);														

	GetWorldTimerManager().SetTimer(AiTimer, this, &AEnemyAIController::GoToRandomWaypoint, 3.0f, false);
	
	TActorIterator<class AEnemyAI> ActorItr(GetWorld());
	UE_LOG(LogTemp, Warning, TEXT("distance: %f"), ActorItr->distanceToPlayer);
}

void AEnemyAIController::BeginPlay()
{
	Super::BeginPlay();

	//UGameplayStatics::GetAllActorsOfClass(GetWorld(), ATargetPoint::StaticClass(), waypoints);				//UGamplayStatics function to get all of the target points in the level
	//waypointArrSize = waypoints.Num();																		//initialising waypoint array to number of waypoints

	//if (currentWaypoint <= waypointArrSize)																	//if current waypoint is smaller than or equal to waypoint array size then do this
	//{
	//	GoToNextWaypoint();																					//calling go to next waypoint
	//}
}

void AEnemyAIController::Tick(float DeltaTime)
{
	Super::Tick(DeltaTime);
	TActorIterator<class AEnemyAI> ActorItr(GetWorld());
	if (ActorItr->distanceToPlayer > AiSightRadius - 10)																			//if distance to player is less than sight radius - 10 so that the value can actually be lower then do this
	{
		ActorItr->bIsPlayerDetected = false;																						//set player detected to false
	}
	else
	{
		ActorItr->bIsPlayerDetected = true;
	}

	//Get Enemy Reference using ActorItr
	//cState->Execute(AEnemyAI);							//calling the abstract version rather than the roam version
	//if (distanceToPlayer > AiSightRadius - 15)									//if player is out of sight radius then do this. minus 15 since the player can't ever reach out the Ai sight radius without it 
	//{
		//bIsPlayerDetected = false;													//set bIsPlayerVisible to false
	//}
}

void AEnemyAIController::PawnDetected(const TArray<AActor*>& pawnsDetected)
{
	TActorIterator<class AEnemyAI> ActorItr(GetWorld());
	for (size_t i = 0; i < pawnsDetected.Num(); i++)																		//runs through all elements in detected pawns array
	{
		ActorItr->distanceToPlayer = GetPawn()->GetDistanceTo(pawnsDetected[i]);														//gets distance from Ai to Player
	}
	//if (ActorItr->distanceToPlayer < AiSightRadius)																					//if player is in sight radius and in peripheral vision do this
	//{
	//	ActorItr->bIsPlayerDetected = true;																							//set bIsPlayerVisible to true
	//}
}

void AEnemyAIController::GoToRandomWaypoint()
{
	MoveToActor(GetRandomWaypoint());
}

ATargetPoint* AEnemyAIController::GetRandomWaypoint()
{
	TActorIterator<class AEnemyAI> ActorItr(GetWorld());
	
	int index = FMath::RandRange(0, ActorItr->waypoints.Num() - 1);												//rand range function in Math.h to get a random number between 0 and number of waypoints - 1 so we dont spill over end of array
	return Cast<ATargetPoint>(ActorItr->waypoints[index]);
}




void AEnemyAIController::GoToNextWaypoint()
{
	TActorIterator<class AEnemyAI> ActorItr(GetWorld());

	MoveToActor(ActorItr->waypoints[ActorItr->currentWaypoint]);								//moves to current specified waypoint
	ActorItr->currentWaypoint++;																//adds one to current waypoint so Ai will always have a new actor to go to
	int Cycle = ActorItr->currentWaypoint % ActorItr->waypointArrSize;							//makes sure we always have a number between min and max of our target points
		
	if (ActorItr->currentWaypoint <= ActorItr->waypointArrSize - 1)								//if current waypoint is less than or equal to waypoint array size - 1 so we don't spill over do this
	{
		ActorItr->currentWaypoint = 0;															//set current waypoint back to start of array
	}
}
