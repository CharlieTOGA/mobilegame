// Fill out your copyright notice in the Description page of Project Settings.


#include "EnemyAI.h"

#include "Kismet/KismetMathLibrary.h"

// Sets default values
AEnemyAI::AEnemyAI(const FObjectInitializer& ObjectInitializer)
	: Super(ObjectInitializer)
{
 	// Set this character to call Tick() every frame.  You can turn this off to improve performance if you don't need it.
	PrimaryActorTick.bCanEverTick = true;

	cState = CreateDefaultSubobject<UAbstractState>(TEXT("current state"));
	rState = CreateDefaultSubobject<UStateRoam>(TEXT("roam state"));
	bIsPlayerDetected = false;
}

void AEnemyAI::StateChange(UAbstractState* newState)
{
	//Get Enemy Reference using ActorItr
	//change Ai state, dependent on each states desirability
	delete cState;
	cState = newState;
	cState->Execute(this);
}

// Called when the game starts or when spawned
void AEnemyAI::BeginPlay()
{
	Super::BeginPlay();
	rState->Execute(this);
}

// Called every frame
void AEnemyAI::Tick(float DeltaTime)
{
	Super::Tick(DeltaTime);


}

// Called to bind functionality to input
void AEnemyAI::SetupPlayerInputComponent(UInputComponent* PlayerInputComponent)
{
	Super::SetupPlayerInputComponent(PlayerInputComponent);

}