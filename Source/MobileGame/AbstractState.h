// Fill out your copyright notice in the Description page of Project Settings.

#pragma once

#include "CoreMinimal.h"
#include "Components/ActorComponent.h"
#include "AbstractState.generated.h"


UCLASS()	// ClassGroup=(Custom), meta=(BlueprintSpawnableComponent) 
class MOBILEGAME_API UAbstractState : public UActorComponent
{
	GENERATED_BODY()

public:	
	virtual void Execute(class AEnemyAI* enemy);
};
