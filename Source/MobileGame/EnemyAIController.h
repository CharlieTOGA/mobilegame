// Fill out your copyright notice in the Description page of Project Settings.

#pragma once

#include "CoreMinimal.h"
#include "AIController.h"
#include "EnemyAIController.generated.h"

/**
 * 
 */
UCLASS()
class MOBILEGAME_API AEnemyAIController : public AAIController
{
	GENERATED_BODY()
	
public:
	AEnemyAIController();

	class AbstractState* cState;
	class StateRoam* rState;
	
	virtual void OnMoveCompleted(FAIRequestID RequestID, const FPathFollowingResult& Result) override;	
	virtual void BeginPlay() override;
	virtual void Tick(float DeltaTime) override;
	void DoDamage();
			

	UPROPERTY(BlueprintReadOnly)
		class UAISenseConfig_Sight* Sight;
	UPROPERTY(BlueprintReadWrite)	
		float AiSightRadius;
	UPROPERTY(BlueprintReadWrite)
		float AiFieldOfView;
	UPROPERTY(BlueprintReadWrite)
		float AiSightAge;	
	UPROPERTY(BlueprintReadWrite)
		float AiOutOfSightRadius;
	UPROPERTY(BlueprintReadWrite)
		float AiConeAngle;

	//UPROPERTY(BlueprintReadOnly)
	//	class UAISenseConfig_Hearing* Hearing;													
	//UPROPERTY(BlueprintReadWrite)											
	//	float AiHearingRadius;																
	//UPROPERTY(BlueprintReadWrite)											
	//	float AiHearingAge;																				

	//UPROPERTY(BlueprintReadOnly)
		//class UAISenseConfig_Touch* Touch;
								


	UFUNCTION()
		void PawnDetected(const TArray<AActor*>& pawnsDetected);
	UFUNCTION()
		void GoToRandomWaypoint();																		
	UFUNCTION()
		ATargetPoint* GetRandomWaypoint();			
	UFUNCTION()
		void GoToNextWaypoint();															

	
																	

	FTimerHandle AiTimer;																		
	FTimerHandle HitTimer;														

																			

};
