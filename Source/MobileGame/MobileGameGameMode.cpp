// Copyright Epic Games, Inc. All Rights Reserved.

#include "MobileGameGameMode.h"
#include "MobileGamePlayerController.h"
#include "MobileGameCharacter.h"
#include "UObject/ConstructorHelpers.h"

AMobileGameGameMode::AMobileGameGameMode()
{
	// use our custom PlayerController class
	PlayerControllerClass = AMobileGamePlayerController::StaticClass();

	// set default pawn class to our Blueprinted character
	static ConstructorHelpers::FClassFinder<APawn> PlayerPawnBPClass(TEXT("/Game/TopDownCPP/Blueprints/TopDownCharacter"));
	if (PlayerPawnBPClass.Class != NULL)
	{
		DefaultPawnClass = PlayerPawnBPClass.Class;
	}
}