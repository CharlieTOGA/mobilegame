// Fill out your copyright notice in the Description page of Project Settings.

#pragma once

#include "CoreMinimal.h"
#include "AbstractState.h"
//#include "StateRoam.h"
#include "StateChase.generated.h"

/**
 * 
 */
UCLASS(meta = (BlueprintSpawnableComponent), Blueprintable)
class MOBILEGAME_API UStateChase : public UAbstractState
{
	GENERATED_BODY()
	
public:

	void Execute(AEnemyAI* Enemy);	//Roam Execute behaviour

	//UStateRoam* stateRoam;
};
